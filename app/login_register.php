<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class login_register extends Model
{
    protected $fillable= ['fname','lname','email','Ph_no','location','gender'];
    public $timestamps = false;
}
